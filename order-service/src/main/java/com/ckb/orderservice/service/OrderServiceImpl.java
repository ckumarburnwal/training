package com.ckb.orderservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.ckb.orderservice.common.dto.OrderDto;
import com.ckb.orderservice.common.dto.Payment;
import com.ckb.orderservice.common.dto.TransactionRequest;
import com.ckb.orderservice.common.dto.TransactionResponse;
import com.ckb.orderservice.entity.Order;
import com.ckb.orderservice.repository.OrderRepository;

@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	private RestTemplate restTemplate;

	@Override
	public TransactionResponse saveOrder(TransactionRequest transactionRequest) {
		
		String response="";
		OrderDto orderDto = transactionRequest.getOrderDto();
		Payment payment = transactionRequest.getPaymentDto();
		
		Order order = createOrder(orderDto);
		
		Order savedOrder = orderRepository.save(order);
		payment.setAmount(savedOrder.getPrice());
		payment.setOrderId(savedOrder.getId());
		System.out.println("test========");
		
		String url="http://payment-service/paymentservice/payment";
		//http://localhost:8082/paymentservice/payment
		
		Payment paymentResponse = restTemplate.postForObject(url, payment, Payment.class);
		TransactionResponse transactionResponse=new TransactionResponse();
		response=paymentResponse.getPaymentStatus().equals("success")?"payment processing successful and order placed":"there is failure in payment api and order failed";
		transactionResponse.setOrderDto(new OrderDto(savedOrder.getId(), savedOrder.getName(), savedOrder.getQty(), savedOrder.getPrice()));
		transactionResponse.setAmount(paymentResponse.getAmount());
		transactionResponse.setMessage(response);
		transactionResponse.setTransactionId(paymentResponse.getTransactionId());
		return transactionResponse;
	}

	private Order createOrder(OrderDto orderDto) {
		Order order=new Order();
		order.setName(orderDto.getName());
		order.setPrice(orderDto.getPrice());
		order.setQty(orderDto.getQty());
		return order;
	}
	
	

}
