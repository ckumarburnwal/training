package com.ckb.paymentservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ckb.paymentservice.entity.Payment;
import com.ckb.paymentservice.service.PaymentService;

@RestController
@RequestMapping("/paymentservice")
public class PaymentController {
	
	@Autowired
	private PaymentService paymentService;
	
	@Autowired
	private Environment environment;
	
	@GetMapping("/test")
	public String getPorts() {
		return environment.getProperty("local.server.port");
	}
	
	@PostMapping("/payment")
	public Payment makePayment(@RequestBody Payment payment) {
		return paymentService.makePayment(payment);
	}
	
	@GetMapping("/payment")
	public List<Payment> findAllPayments(){
		return paymentService.findAllPayments();
	}
	
	@GetMapping("/payment/{orderId}")
	public Payment findPaymentById(@PathVariable Long orderId){
		return paymentService.findPaymentByOrderId(orderId);
	}

}
