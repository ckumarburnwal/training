package com.ckb.upipayment.repository;

import org.springframework.data.repository.CrudRepository;

import com.ckb.upipayment.entity.UPIAccount;

public interface UPIRepository extends CrudRepository<UPIAccount, Long>{

	
	UPIAccount findUPIAccountByPhone(String phone);

}
