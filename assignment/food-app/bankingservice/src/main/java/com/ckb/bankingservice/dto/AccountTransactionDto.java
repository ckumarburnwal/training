package com.ckb.bankingservice.dto;

import java.math.BigDecimal;
import java.util.Date;


public class AccountTransactionDto {
	
	
    private Long transactionId;
	
	
    private Date transactionCreated;
	
	
    private String transactionType;
    
	
    private String transactionStatus;
    
	
    private BigDecimal transactionAmount;
    
	
    private BigDecimal availableBalance;
    
	
	
	public AccountTransactionDto() {
		
	}

	public Long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	public Date getTransactionCreated() {
		return transactionCreated;
	}

	public void setTransactionCreated(Date transactionCreated) {
		this.transactionCreated = transactionCreated;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	public BigDecimal getTransactionAmount() {
		return transactionAmount;
	}

	public void setTransactionAmount(BigDecimal transactionAmount) {
		this.transactionAmount = transactionAmount;
	}

	public BigDecimal getAvailableBalance() {
		return availableBalance;
	}

	public void setAvailableBalance(BigDecimal availableBalance) {
		this.availableBalance = availableBalance;
	}

	
	
	

}
