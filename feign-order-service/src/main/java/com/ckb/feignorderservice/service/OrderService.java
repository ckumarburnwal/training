package com.ckb.feignorderservice.service;

import com.ckb.feignorderservice.dto.OrderDetails;
import com.ckb.feignorderservice.dto.TransactionRequest;
import com.ckb.feignorderservice.dto.TransactionResponse;

public interface OrderService {

	public TransactionResponse saveOrder(TransactionRequest transactionRequest);

	public OrderDetails getOrderDetails(Long orderId);
}
