package com.ckb.paymentservice.service;

import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ckb.paymentservice.entity.Payment;
import com.ckb.paymentservice.repository.PaymentRepository;

@Service
public class PaymentServiceImpl implements PaymentService {
	
	@Autowired
	private PaymentRepository paymentRepository;

	@Override
	public Payment makePayment(Payment payment) {
		payment.setPaymentStatus(paymentProcessing());
		payment.setTransactionId(UUID.randomUUID().toString());
		return paymentRepository.save(payment);
	}
	
	public String paymentProcessing() {
		//api third party payment gatway like paypal,paytm
		return new Random().nextBoolean()? "success": "false";
	}

	@Override
	public List<Payment> findAllPayments() {
		return (List<Payment>) paymentRepository.findAll();
	}

	@Override
	public Payment findPaymentByOrderId(Long orderId) {
		// TODO Auto-generated method stub
		System.out.println(orderId);
		return paymentRepository.findPaymentByOrderId(orderId);
	}

}
