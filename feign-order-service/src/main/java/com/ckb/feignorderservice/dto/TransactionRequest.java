package com.ckb.feignorderservice.dto;

public class TransactionRequest {
	
	private OrderDto orderDto;
	private Payment paymentDto;
	
	public TransactionRequest() {
		
	}
	public OrderDto getOrderDto() {
		return orderDto;
	}
	public void setOrderDto(OrderDto orderDto) {
		this.orderDto = orderDto;
	}
	public Payment getPaymentDto() {
		return paymentDto;
	}
	public void setPaymentDto(Payment paymentDto) {
		this.paymentDto = paymentDto;
	}

}
