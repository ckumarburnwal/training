package com.ckb.feignorderservice.feignclient;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.ckb.feignorderservice.dto.Payment;


@FeignClient(name="payment-service",path = "/paymentservice")
public interface PaymentServiceClient {
	
	@PostMapping("/payment")
	public Payment makePayment(@RequestBody Payment payment);
	
	@GetMapping("/payment")
	public List<Payment> findAllPayments();
	
	@GetMapping("/payment/{orderId}")
	public Payment findPaymentById(@PathVariable Long orderId);
	
	@GetMapping("/test")
	public String getPorts();

}
