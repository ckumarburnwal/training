package com.ckb.feignorderservice.repository;


import org.springframework.data.repository.CrudRepository;

import com.ckb.feignorderservice.entity.Order;

public interface OrderRepository extends CrudRepository<Order, Long>{

}
