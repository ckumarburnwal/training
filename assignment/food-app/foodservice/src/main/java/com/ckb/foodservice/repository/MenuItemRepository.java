package com.ckb.foodservice.repository;

import org.springframework.data.repository.CrudRepository;

import com.ckb.foodservice.entity.MenuItem;

public interface MenuItemRepository extends CrudRepository<MenuItem, String> {

}
