package com.ckb.bankingservice.repository;

import org.springframework.data.repository.CrudRepository;

import com.ckb.bankingservice.entity.User;

public interface UserRepository extends CrudRepository<User, Long> {

	
	User findUserByPhone(String phone);
}
