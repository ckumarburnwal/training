package com.ckb.foodservice.service;

import java.util.List;

import com.ckb.foodservice.dto.MenuItemResponse;
import com.ckb.foodservice.dto.UserRequestDto;
import com.ckb.foodservice.dto.UserResponseDto;

public interface UserService {
	
	UserResponseDto registerUser(UserRequestDto userDto);
	
	List<MenuItemResponse> findMenuItems(String menuItem);
	List<MenuItemResponse> findMenuItemsForVendor(String vendor);
	

}
