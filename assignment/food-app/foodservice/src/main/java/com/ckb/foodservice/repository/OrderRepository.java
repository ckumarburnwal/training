package com.ckb.foodservice.repository;

import org.springframework.data.repository.CrudRepository;

import com.ckb.foodservice.entity.UserOrder;

public interface OrderRepository extends CrudRepository<UserOrder, String> {

}
