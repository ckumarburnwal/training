package com.ckb.orderservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ckb.orderservice.common.dto.TransactionRequest;
import com.ckb.orderservice.common.dto.TransactionResponse;
import com.ckb.orderservice.service.OrderService;

@RestController
@RequestMapping("/orderservice")
public class OrderController {
	
	@Autowired
	private OrderService orderService;
	
	@PostMapping("/orders")
	public TransactionResponse createOrder(@RequestBody TransactionRequest transactionRequest) {
		return orderService.saveOrder(transactionRequest);
	}

}
