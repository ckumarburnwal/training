package com.ckb.feignorderservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

import com.ckb.feignorderservice.conf.PaymentServiceConfiguration;

@SpringBootApplication
@EnableFeignClients
@RibbonClient(name = "payment-client",configuration = PaymentServiceConfiguration.class)
@EnableEurekaClient
public class FeignOrderServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(FeignOrderServiceApplication.class, args);
	}

}
