package com.ckb.foodservice.repository;

import org.springframework.data.repository.CrudRepository;

import com.ckb.foodservice.entity.FoodVendor;

public interface VendorRepository extends CrudRepository<FoodVendor, String> {
	
	FoodVendor findByNameContainingIgnoreCase(String vendor);

}
