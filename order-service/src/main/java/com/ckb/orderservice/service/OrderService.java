package com.ckb.orderservice.service;

import com.ckb.orderservice.common.dto.TransactionRequest;
import com.ckb.orderservice.common.dto.TransactionResponse;

public interface OrderService {

	public TransactionResponse saveOrder(TransactionRequest transactionRequest);
}
