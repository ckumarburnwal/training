package com.ckb.foodservice.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.ckb.foodservice.entity.User;


public interface UserRepository extends PagingAndSortingRepository<User, String> {

}
