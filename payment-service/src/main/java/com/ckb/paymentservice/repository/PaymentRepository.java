package com.ckb.paymentservice.repository;

import org.springframework.data.repository.CrudRepository;

import com.ckb.paymentservice.entity.Payment;

public interface PaymentRepository extends CrudRepository<Payment, Integer> {

	Payment findPaymentByOrderId(Long orderId);

}
