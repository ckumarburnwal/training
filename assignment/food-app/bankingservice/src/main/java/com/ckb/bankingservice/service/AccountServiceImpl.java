package com.ckb.bankingservice.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ckb.bankingservice.dto.AccountDetail;
import com.ckb.bankingservice.dto.AccountTransactionDto;
import com.ckb.bankingservice.dto.PaymentRequest;
import com.ckb.bankingservice.entity.Account;
import com.ckb.bankingservice.entity.AccountTransaction;
import com.ckb.bankingservice.entity.User;
import com.ckb.bankingservice.repository.AccountRepository;
import com.ckb.bankingservice.repository.AccountTransactionRepository;
import com.ckb.bankingservice.repository.UserRepository;

@Service
public class AccountServiceImpl implements AccountService {

	@Autowired
	private AccountRepository accountRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private AccountTransactionRepository accountTransactionRepository;
	
	
	@Override
	@Transactional
	public String makePayment(PaymentRequest paymentRequest, String userId) {
     
		Optional<User> optionalUser = userRepository.findById(userId);
		if(optionalUser.isPresent()) {
			User user = optionalUser.get();
			Account account = user.getAccounts().get(0);
			boolean isCardDetailVaid = validateCardDetails(account,paymentRequest);
			if(isCardDetailVaid) {
				
				account.setAccountBalance(account.getAccountBalance().subtract(paymentRequest.getAmount()));
				AccountTransaction debit=createTransactions(account,paymentRequest);
		       
			    account.getTransactions().add(debit);
				accountRepository.save(account);
				
			} else {
				throw new RuntimeException("card details not valid");
			}
		}
		else {
			throw new RuntimeException(userId+ "user not found");
		}
		return "success";
	}


	private AccountTransaction createTransactions(Account account, PaymentRequest paymentRequest) {
		AccountTransaction transaction=new AccountTransaction();
		transaction.setAccount(account);
		transaction.setAvailableBalance(account.getAccountBalance());
		transaction.setTransactionAmount(paymentRequest.getAmount());
		transaction.setTransactionCreated(new Date());
		transaction.setTransactionStatus("success");
		transaction.setTransactionType("debit");
		return transaction;
	}


	private boolean validateCardDetails(Account account, PaymentRequest paymentRequest) {
		boolean isCardValid=false;
		Account accountToCompare=new Account();
		accountToCompare.setCardNumber(paymentRequest.getCardNumber());	
		accountToCompare.setCvv(paymentRequest.getCvv());
		accountToCompare.setExpirationDate(paymentRequest.getExpirationDate());
		if(accountToCompare.equals(accountToCompare)) {
			isCardValid=true;
		}
		else {
			isCardValid=false;
		}
		return isCardValid;
	}


	@Override
	public List<AccountTransactionDto> findTransactions(String userId) {
		
		Optional<User> optionalUser = userRepository.findById(userId);
		List<AccountTransactionDto> transactions=null;
		if(optionalUser.isPresent()) {
			User user = optionalUser.get();
			Account account = user.getAccounts().get(0);
			 List<AccountTransaction> accountTransactions = account.getTransactions().stream().limit(5).collect(Collectors.toList());
			 transactions=createTransactionsDtosList(accountTransactions);
			
		} else {
			throw new RuntimeException("userId not found");
		}
		return transactions;
	}


	@Override
	public AccountDetail getAccountDetails(String userId) {
		Optional<User> optionalUser = userRepository.findById(userId);
		AccountDetail accountDetail=new AccountDetail();
		if(optionalUser.isPresent()) {
			User user = optionalUser.get();
			Account account = user.getAccounts().get(0);
			accountDetail.setAccountBalance(account.getAccountBalance());
			accountDetail.setAccountNumber(account.getAccountNumber());
			accountDetail.setCardNumber(account.getCardNumber());
			accountDetail.setCvv(account.getCvv());
			accountDetail.setExpirationDate(account.getExpirationDate());
		} else {
			throw new RuntimeException("userId not found");
		}
		return accountDetail;
	}


	@Override
	public List<AccountTransactionDto>  getLastMonthTransactions(String userId) {
		Optional<User> optionalUser = userRepository.findById(userId);
		List<AccountTransactionDto> transactions=null;
		if(optionalUser.isPresent()) {
			User user = optionalUser.get();
			String accountNumber = user.getAccounts().get(0).getAccountNumber();			
			LocalDate today =  LocalDate.now();
			LocalDate lastMonthDate = today.minusMonths(1);
			ZoneId defaultZoneId = ZoneId.systemDefault();
	        Date date = Date.from(lastMonthDate.atStartOfDay(defaultZoneId).toInstant());

			 System.out.println("date======>"+date);
			 List<AccountTransaction> accountTransactions = accountTransactionRepository.findAAllTransactions(accountNumber,date);
			 transactions=createTransactionsDtosList(accountTransactions);
			 
		} else {
			throw new RuntimeException("userId not found");
		}
		return transactions;
	
	}


	private List<AccountTransactionDto> createTransactionsDtosList(List<AccountTransaction> accountTransactions) {
		return accountTransactions.stream().map(transaction -> {
			 AccountTransactionDto transactionDto=new AccountTransactionDto();
			 transactionDto.setAvailableBalance(transaction.getAvailableBalance());
			 transactionDto.setTransactionAmount(transaction.getTransactionAmount());
			 transactionDto.setTransactionCreated(transaction.getTransactionCreated());
			 transactionDto.setTransactionId(transaction.getTransactionId());
			 transactionDto.setTransactionStatus(transaction.getTransactionStatus());
			 transactionDto.setTransactionType(transaction.getTransactionType());
			 return transactionDto;
		 } ).collect(Collectors.toList());
	}
	
	

}
