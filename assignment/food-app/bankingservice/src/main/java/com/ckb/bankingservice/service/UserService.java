package com.ckb.bankingservice.service;

import com.ckb.bankingservice.dto.UserRequestDto;
import com.ckb.bankingservice.dto.UserResponseDto;

public interface UserService {
	
	UserResponseDto registerUser(UserRequestDto userDto);
	

}
