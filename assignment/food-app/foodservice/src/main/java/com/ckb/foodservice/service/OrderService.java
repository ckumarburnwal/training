package com.ckb.foodservice.service;

import com.ckb.foodservice.dto.OrderRequestDto;
import com.ckb.foodservice.dto.OrderResponse;

public interface OrderService {
	
	OrderResponse createOrder(OrderRequestDto orderRequestDto, String userId, String vendor);

	OrderResponse getOrderHistory(String userId);

}
