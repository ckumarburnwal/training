package com.ckb.bankingservice.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import com.ckb.bankingservice.entity.AccountTransaction;

public interface AccountTransactionRepository extends PagingAndSortingRepository<AccountTransaction, Long> {
	
    @Query("FROM AccountTransaction ac where ac.account.accountNumber = :accountNumber and ac.transactionCreated >=:date")
    List<AccountTransaction> findAAllTransactions(@Param("accountNumber") String accountNumString,@Param("date") Date date);

}
