package com.ckb.bankingservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ckb.bankingservice.dto.AccountDetail;
import com.ckb.bankingservice.dto.AccountTransactionDto;
import com.ckb.bankingservice.dto.PaymentRequest;
import com.ckb.bankingservice.service.AccountService;

@RestController
@RequestMapping("/bankingservice")
public class BankingServiceController {

	@Autowired
	private AccountService accountService;
	
	@PostMapping("/accounts/{userId}")
	public String makePayment(
			@RequestBody PaymentRequest paymentRequest,
			@PathVariable String userId){
		
		return accountService.makePayment(paymentRequest,userId);		
	}
	
	@GetMapping("/accounts/{userId}/transactions")
	public List<AccountTransactionDto> findTransactions(@PathVariable String userId) {
		return accountService.findTransactions(userId);
	}
	
	
	@GetMapping("/accounts/{userId}")
	public AccountDetail getAccountDetails(@PathVariable String userId) {
		return accountService.getAccountDetails(userId);
	}
	
	@GetMapping("/accounts/{userId}/statements")
	public List<AccountTransactionDto> getMonthlyStatement(@PathVariable String userId) {
		
		return accountService.getLastMonthTransactions(userId);
		
			}
}
