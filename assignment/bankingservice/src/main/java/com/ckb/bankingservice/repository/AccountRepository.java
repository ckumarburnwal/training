package com.ckb.bankingservice.repository;

import org.springframework.data.repository.CrudRepository;

import com.ckb.bankingservice.entity.Account;

public interface AccountRepository extends CrudRepository<Account, Long> {

}
