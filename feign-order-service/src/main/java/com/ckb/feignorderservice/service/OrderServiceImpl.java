package com.ckb.feignorderservice.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ckb.feignorderservice.dto.OrderDetails;
import com.ckb.feignorderservice.dto.OrderDto;
import com.ckb.feignorderservice.dto.Payment;
import com.ckb.feignorderservice.dto.TransactionRequest;
import com.ckb.feignorderservice.dto.TransactionResponse;
import com.ckb.feignorderservice.entity.Order;
import com.ckb.feignorderservice.feignclient.PaymentServiceClient;
import com.ckb.feignorderservice.repository.OrderRepository;

@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	private OrderRepository orderRepository;	
	@Autowired
	PaymentServiceClient paymentServiceClient;

	@Override
	public TransactionResponse saveOrder(TransactionRequest transactionRequest) {
		
		String response="";
		OrderDto orderDto = transactionRequest.getOrderDto();
		Payment payment = transactionRequest.getPaymentDto();
		
		Order order = createOrder(orderDto);
		
		Order savedOrder = orderRepository.save(order);
		payment.setAmount(savedOrder.getPrice());
		payment.setOrderId(savedOrder.getId());
		System.out.println("test========");
		Payment paymentResponse = paymentServiceClient.makePayment(payment);
		TransactionResponse transactionResponse=new TransactionResponse();
		response=paymentResponse.getPaymentStatus().equals("success")?"payment processing successful and order placed":"there is failure in payment api and order failed";
		transactionResponse.setOrderDto(new OrderDto(savedOrder.getId(), savedOrder.getName(), savedOrder.getQty(), savedOrder.getPrice()));
		transactionResponse.setAmount(paymentResponse.getAmount());
		transactionResponse.setMessage(response);
		transactionResponse.setTransactionId(paymentResponse.getTransactionId());
		return transactionResponse;
	}

	private Order createOrder(OrderDto orderDto) {
		Order order=new Order();
		order.setName(orderDto.getName());
		order.setPrice(orderDto.getPrice());
		order.setQty(orderDto.getQty());
		return order;
	}

	@Override
	public OrderDetails getOrderDetails(Long orderId) {
		Optional<Order> findById = orderRepository.findById(orderId);
		OrderDetails orderDetails=new OrderDetails();
		if(findById.isPresent()) {
			Order order = findById.get();
			Payment payment = paymentServiceClient.findPaymentById(orderId);
			
			orderDetails.setOrderStatus(payment.getPaymentStatus());
			orderDetails.setTransactionId(payment.getTransactionId());
			orderDetails.setOrderDto(new OrderDto(order.getId(), order.getName(), order.getQty(), order.getPrice()));
			orderDetails.setAmount(payment.getAmount());
		}
		return orderDetails;
	}
	
	

}
