package com.ckb.paymentservice.service;

import java.util.List;

import com.ckb.paymentservice.entity.Payment;

public interface PaymentService {

	public Payment makePayment(Payment payment);

	public List<Payment> findAllPayments();

	public Payment findPaymentByOrderId(Long orderId);
}
