package com.ckb.bankingservice.service;

import java.util.List;

import com.ckb.bankingservice.dto.AccountDetail;
import com.ckb.bankingservice.dto.AccountTransactionDto;
import com.ckb.bankingservice.dto.PaymentRequest;

public interface AccountService {
	
	String makePayment(PaymentRequest paymentRequest,String userId);
	List<AccountTransactionDto> findTransactions(String userId);
	AccountDetail getAccountDetails(String userId);
	List<AccountTransactionDto> getLastMonthTransactions(String userId);
	

}
