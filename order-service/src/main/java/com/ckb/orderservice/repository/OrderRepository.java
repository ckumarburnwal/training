package com.ckb.orderservice.repository;

import org.springframework.data.repository.CrudRepository;

import com.ckb.orderservice.entity.Order;

public interface OrderRepository extends CrudRepository<Order, Long>{

}
