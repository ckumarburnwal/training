package com.ckb.feignorderservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ckb.feignorderservice.dto.OrderDetails;
import com.ckb.feignorderservice.dto.TransactionRequest;
import com.ckb.feignorderservice.dto.TransactionResponse;
import com.ckb.feignorderservice.feignclient.PaymentServiceClient;
import com.ckb.feignorderservice.service.OrderService;



@RestController
@RequestMapping("/orderservice")
public class OrderController {
	
	@Autowired
	private OrderService orderService;
	
	@Autowired
	PaymentServiceClient paymentServiceClient;
	
	@PostMapping("/orders")
	public TransactionResponse createOrder(@RequestBody TransactionRequest transactionRequest) {
		return orderService.saveOrder(transactionRequest);
	}
	
	@GetMapping("/orders/{orderId}")
	public OrderDetails getOrderDetails(@PathVariable Long orderId) {
		  
		return orderService.getOrderDetails(orderId);
				
	}
	
	@GetMapping("/test")
	public String getServerPort() {
		  
		return paymentServiceClient.getPorts();
				
	}
	
	
	 

}
