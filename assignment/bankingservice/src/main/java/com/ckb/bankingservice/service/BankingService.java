package com.ckb.bankingservice.service;

import com.ckb.bankingservice.dto.AccountDetail;
import com.ckb.bankingservice.dto.TransferAmountDTO;

public interface BankingService {

	AccountDetail findAccountDetailByPhone(String phone);
	
	String transfer(String fromPhone, String toPhone,TransferAmountDTO transferAmountDTO);
}
