package com.ckb.upipayment.service;

import java.util.List;

import com.ckb.upipayment.dto.TransferAmountDTO;
import com.ckb.upipayment.dto.UPIAccountDetail;
import com.ckb.upipayment.entity.UPIAccountTransaction;

public interface UPIService {

	UPIAccountDetail registerUPIAccount(String phone);
	String transferAmount(TransferAmountDTO amountDTo,String fromPhone,String toPhone); 
	List<UPIAccountTransaction> getTransactions(String phone);

}
