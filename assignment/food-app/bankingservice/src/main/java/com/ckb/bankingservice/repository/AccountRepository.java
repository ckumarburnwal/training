package com.ckb.bankingservice.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.ckb.bankingservice.entity.Account;

public interface AccountRepository extends PagingAndSortingRepository<Account, String> {

}
